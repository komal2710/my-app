import React, { Component } from 'react';
import './App.css';

import DisplayCount from './components/DisplayCount';
import Chart from './components/Chart';
import Chart1 from './components/Chart1';
import ChartSMA from './components/ChartSMA';
import ChartPer from './components/ChartPer'

import jsondata from './jsonData.json';

class App extends Component {
  state = {
    statewise: [],
    casesTimeSeries: [],
    chart1: "show",
    chart2: "hide",
  }

  componentDidMount() {
    fetch('https://api.covid19india.org/data.json')
      .then(res => res.json())
      .then((data) => {
        this.setState({ statewise: data.statewise });
        this.setState({ casesTimeSeries: data.cases_time_series });
      })
      .catch(console.log);
  }

  setTimeseriesMode = () => {
    console.log(this.state.chart1)
    if (this.state.chart1 == "show") {
      this.setState({ chart1: "hide", chart2: "show" });
    } else {
      this.setState({ chart1: "show", chart2: "hide" });
    }
  }

  render() {
    console.log(this.state.casesTimeSeries);
    return (
      <div className="App">
        <header className="App-header">
          <DisplayCount statewise={this.state.statewise} casesTimeSeries={this.state.casesTimeSeries} />
          <div className="AdvanceChartOption">
            <span className="react-switch-checkbox"
              onClick={(event) => { this.setTimeseriesMode() }}>More Charts >> </span>
          </div>

          <div className="ChartDiv">
            <div className={"ChartDiv1 " + this.state.chart1}>
              <div className="row">
                <div className="column"><Chart casesTimeSeries={this.state.casesTimeSeries} /></div>
                <div className="column"><Chart1 casesTimeSeries={this.state.casesTimeSeries} /></div>
              </div>
            </div>
            <div className={"ChartDiv2 " + this.state.chart2}>
              <div className="row">
                <div className="column"><ChartSMA casesTimeSeries={this.state.casesTimeSeries} /></div>
                <div className="column"><ChartPer casesTimeSeries={this.state.casesTimeSeries} /></div>
              </div>
            </div>
          </div>
        </header>

      </div>
    );
  };
}

export default App;
