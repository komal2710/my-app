import React from 'react';
import CanvasJSReact from './../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;


const Chart = ({ casesTimeSeries }) => {

	var totalconfirmedData = [];
	var totalrecoveredData = [];
	var totaldeceasedData = [];
	var activeData = [];
	var minimumValue = 1;

	if (casesTimeSeries && casesTimeSeries.length > 0) {
		casesTimeSeries = casesTimeSeries.slice(casesTimeSeries.length - 31, casesTimeSeries.length);
	}

	casesTimeSeries.map((item, index) => {
		if (index == 0) {
			minimumValue = item.totaldeceased;
		}
		let date = new Date(item.date);
		let active = Number(item.totalconfirmed);

		totalconfirmedData.push({ x: date, y: Number(item.totalconfirmed) });

		if (Number(item.totalrecovered) != 0) {
			totalrecoveredData.push({ x: date, y: Number(item.totalrecovered) });
			active -= Number(item.totalrecovered);
		}

		if (Number(item.totaldeceased) != 0) {
			totaldeceasedData.push({ x: date, y: Number(item.totaldeceased) });
			active -= Number(item.totaldeceased);
		}

		if (active != 0) {
			activeData.push({ x: date, y: active });
		}
	});

	const options = {
		animationEnabled: true,
		theme: "dark1",
		axisX: {
			valueFormatString: "DD MMM"
		},
		axisY: {
			title: "Count",
			includeZero: false,
			logarithmic: true,
			gridThickness: 1,
			gridDashType: "dot",
			gridColor: "gray",
			minimum : minimumValue
		},
		height: 300,
		data: [{
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "Confirmed",
			color: "chocolate",
			dataPoints: totalconfirmedData
		},
		{
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "Active",
			color: "#007bff",
			dataPoints: activeData
		},
		{
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "Recovered",
			color: "green",
			dataPoints: totalrecoveredData
		}, {
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "Deaths",
			color: "red",
			dataPoints: totaldeceasedData
		}]
	}
	return (
		<div className="ChartWithLogarithmicAxis">
			<CanvasJSChart options={options} />
		</div>
	);

};

export default Chart;