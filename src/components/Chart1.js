import React from 'react';
import CanvasJSReact from './../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

function getweeklyCount(casesTimeSeries, index, att) {
	var weeklyCount = 0;

	for (var i = index; i >= 0 && i > index - 7; i--) {
		weeklyCount += Number(casesTimeSeries[i][att])
	}
	return weeklyCount;
}


const Chart = ({ casesTimeSeries }) => {

	var totalconfirmedData = [];
	var totalrecoveredData = [];
	var totaldeceasedData = [];
	var activeData = [];

	if (casesTimeSeries && casesTimeSeries.length > 0) {
		casesTimeSeries = casesTimeSeries.slice(0, casesTimeSeries.length);
	}

	casesTimeSeries.map((item, index) => {
		let date = new Date(item.date);
		let active = Number(item.totalconfirmed);

		var dailyconfirmed = getweeklyCount(casesTimeSeries, index, "dailyconfirmed");
		if (dailyconfirmed > 0) {
			totalconfirmedData.push({ x: Number(item.totalconfirmed), y: dailyconfirmed });
		}

		var dailyrecovered = getweeklyCount(casesTimeSeries, index, "dailyrecovered");
		if (dailyrecovered) {
			totalrecoveredData.push({ x: Number(item.totalconfirmed), y: dailyrecovered });
		}

		var dailydeceased = getweeklyCount(casesTimeSeries, index, "dailydeceased");
		if (dailydeceased) {
			totaldeceasedData.push({ x: Number(item.totalconfirmed), y: dailydeceased });
		}
	});

	console.log(totalconfirmedData);

	const options = {
		animationEnabled: true,
		theme: "dark1",
		axisX: {
			title: "Confirmed",
			logarithmic: true,
			includeZero: false,
			minimum : "10"
		},
		axisY: {
			title: "Weekly Count",
			includeZero: false,
			logarithmic: true,
			minimum : "1",
			gridThickness: 1,
			gridDashType : "dot",
			gridColor : "gray",
		},
		height: 300,
		data: [{
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "Confirmed",
			color: "chocolate",
			dataPoints: totalconfirmedData
		},
		{
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "Recovered",
			color: "green",
			dataPoints: totalrecoveredData
		}, {
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "Deaths",
			color: "red",
			dataPoints: totaldeceasedData
		}]
	}
	return (
		<div className="ChartWithLogarithmicAxis">
			<CanvasJSChart options={options} />
		</div>
	);

};

export default Chart;