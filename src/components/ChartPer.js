import React from 'react';
import CanvasJSReact from './../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

function getPer(confirmed, count) {
	var result = 0;

	result = count * 100 / confirmed;

	return result;
}


const Chart = ({ casesTimeSeries }) => {

	var activeData = [];
	var per1Data = [];
	var per2Data = [];
	var per3Data = [];

	var minimumValue = 1;

	if (casesTimeSeries && casesTimeSeries.length > 0) {
		casesTimeSeries = casesTimeSeries.slice(casesTimeSeries.length - 31, casesTimeSeries.length);
	}

	casesTimeSeries.map((item, index) => {

		let date = new Date(item.date);
		let totalconfirmed = Number(item.totalconfirmed);
		let totalrecovered = Number(item.totalrecovered);
		let totaldeceased = Number(item.totaldeceased);

		var per1 = getPer(totalconfirmed, totalrecovered);
		if (per1) {
			per1Data.push({ x: date, y: per1 });
		}

		var per2 = getPer(totalconfirmed, totaldeceased);
		if (per2) {
			per2Data.push({ x: date, y: per2 });
		}

		var per3 = 100 - (per1 + per2);
		if (per3) {
			per3Data.push({ x: date, y: per3 });
		}
	});

	const options = {
		animationEnabled: true,
		theme: "dark1",
		axisX: {
			valueFormatString: "DD MMM",
		},
		axisY: {
			title: "Percentage",
			includeZero: false,
			logarithmic: false,
			gridThickness: 1,
			gridDashType: "dot",
			gridColor: "gray",
			minimum: minimumValue
		},
		legend: {
			cursor: "pointer",
			itemclick: function (e) {
				if (typeof (e.dataSeries.visible) === "undefined" || e.dataSeries.visible) {
					e.dataSeries.visible = false;
				}else {
                    e.dataSeries.visible = true;
                }
                e.chart.render();
			}
		},
		height: 300,
		data: [
			{
				type: "spline",
				showInLegend: true,
				markerType: "none",
				legendText: "Active",
				color: "chocolate",
				dataPoints: per3Data
			},
			{
				type: "spline",
				showInLegend: true,
				markerType: "none",
				legendText: "Recovered",
				color: "green",
				dataPoints: per1Data
			}, {
				type: "spline",
				showInLegend: true,
				markerType: "none",
				legendText: "Deaths",
				color: "red",
				dataPoints: per2Data
			}
		]
	}
	return (
		<div className="ChartWithLogarithmicAxis">
			<CanvasJSChart options={options} />
		</div>
	);

};

export default Chart;