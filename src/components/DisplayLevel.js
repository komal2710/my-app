import React from 'react'
import MiniChart from './MiniChart'

const DisplayLevel = ({ level, count, delta, casesTimeSeries, attName }) => {
    return (
        <div className={"item-level " + level}>
            <h5> {level}</h5>
            <h4> {delta ? "["+delta+"]" : ""}&nbsp;</h4>
            <h1> {count}</h1>
            <MiniChart casesTimeSeries={casesTimeSeries} attName={attName} />
        </div>
    )
};

export default DisplayLevel