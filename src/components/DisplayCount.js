import React from 'react'
import DisplayLevel from './DisplayLevel'


const DisplayCount = ({ statewise, casesTimeSeries }) => {

    let state = {
        active: 0,
        confirmed: 0,
        deaths: 0,
        deltaconfirmed: 0,
        deltadeaths: 0,
        deltarecovered: 0,
        recovered: 0,
        miniChartData: []
    }

    if (statewise && statewise.length > 0) {
        state.active = statewise[0].active;
        state.confirmed = statewise[0].confirmed;
        state.deaths = statewise[0].deaths;
        state.recovered = statewise[0].recovered;
        state.deltaconfirmed = statewise[0].deltaconfirmed;
        state.deltarecovered = statewise[0].deltarecovered
        state.deltadeaths = statewise[0].deltadeaths;
    }


    return (
        <div className="Level">
            <DisplayLevel level="confirmed" count={state.confirmed} delta={state.deltaconfirmed} casesTimeSeries={casesTimeSeries} attName="dailyconfirmed" />
            <DisplayLevel level="active" count={state.active} delta="" casesTimeSeries={casesTimeSeries} attName="active" />
            <DisplayLevel level="recovered" count={state.recovered} delta={state.deltarecovered} casesTimeSeries={casesTimeSeries} attName="dailyrecovered" />
            <DisplayLevel level="deaths" count={state.deaths} delta={state.deltadeaths} casesTimeSeries={casesTimeSeries} attName="dailydeceased" />
        </div>
    )
};

export default DisplayCount