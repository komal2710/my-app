import React, { useState, useEffect, useRef, useCallback } from 'react';
import * as d3 from 'd3';

const MiniChart = ({ casesTimeSeries, attName }) => {

    let graphElement1 = useRef(null);



    if (casesTimeSeries && casesTimeSeries.length > 0) {
        casesTimeSeries = casesTimeSeries.slice(casesTimeSeries.length - 20, casesTimeSeries.length);


        let data = casesTimeSeries;
        let svg1 = d3.select(graphElement1.current);
        let margin = { top: 30, right: 10, bottom: 30, left: 0 };
        let width = 100 - margin.left - margin.right;
        let height = 100 - margin.top - margin.bottom;

        let x = d3
            .scaleTime()
            .domain(
                d3.extent(data, function (d) {
                    return new Date(d['date']);
                })
            )
            .range([0, width]);

        let y1 = d3
            .scaleLinear()
            .domain([
                0,
                d3.max(data, function (d) {
                    if (attName === 'active') {
                        return + d['dailyconfirmed']
                    } else {
                        return + d[attName];
                    }
                }),
            ])
            .range([height, 0]);

        svg1
            .append('path')
            .datum(data)
            .attr('fill', 'none')
            .attr('stroke', '#ff073a99')
            .attr('stroke-width', 2)
            .attr('cursor', 'pointer')
            .transition()
            .attr('stroke-dashoffset', 0)
            .attr(
                'd',
                d3
                    .line()
                    .x(function (d) {
                        return x(new Date(d['date']));
                    })
                    .y(function (d, i) {
                        if (attName === 'active') {
                            return y1(d['dailyconfirmed'] - d['dailyrecovered'] - d['dailydeceased'])
                        } else {
                            return y1(d[attName]);
                        }
                    })
                    .curve(d3.curveCardinal)
            );
    }

    return (
        <div>
            <svg
                ref={graphElement1}
                width="100"
                height="100"
                viewBox="0 0 100 100"
                preserveAspectRatio="xMidYMid meet"
            />
        </div>
    );

};

export default MiniChart;