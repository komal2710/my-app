import React from 'react';
import CanvasJSReact from './../assets/canvasjs.react';
var CanvasJSChart = CanvasJSReact.CanvasJSChart;

function getSMACount(casesTimeSeries, index, sma) {
	var smaCount = 0;

	for (var i = index; i >= 0 && i > index - sma; i--) {
		smaCount += Number(casesTimeSeries[i]["dailyconfirmed"])
	}
	return smaCount / sma;
}


const Chart = ({ casesTimeSeries }) => {

	var confirmedData = [];
	var sma1Data = [];
	var sma2Data = [];
	var sma3Data = [];
	var acviteData = [];
	var minimumValue = 1;

	if (casesTimeSeries && casesTimeSeries.length > 0) {
		casesTimeSeries = casesTimeSeries.slice(casesTimeSeries.length - 31, casesTimeSeries.length);
	}

	casesTimeSeries.map((item, index) => {

		let date = new Date(item.date);
		let confirmed = Number(item.dailyconfirmed);

		if (confirmed > 0) {
			confirmedData.push({ x: date, y: confirmed });
		}

		var sma1 = getSMACount(casesTimeSeries, index, 4);
		if (sma1) {
			sma1Data.push({ x: date, y: sma1 , lineDashType : "dash"});
		}

		var sma2 = getSMACount(casesTimeSeries, index, 7);
		if (sma2) {
			sma2Data.push({ x: date, y: sma2 , lineDashType : "dash"});
		}
		var sma3 = getSMACount(casesTimeSeries, index, 14);
		if (sma3) {
			sma3Data.push({ x: date, y: sma3 , lineDashType : "dash"});
		}

		var active = Number(item.dailyconfirmed);
		active -= Number(item.dailydeceased);
		active -= Number(item.dailyrecovered);

		if(active) {
			acviteData.push({ x: date, y: active});
		}

		if (index == 0) {
			minimumValue = sma3;
		}
	});

	const options = {
		animationEnabled: true,
		theme: "dark1",
		axisX: {
			valueFormatString: "DD MMM",
		},
		axisY: {
			title: "Confirmed",
			includeZero: false,
			logarithmic: false,
			gridThickness: 1,
			gridDashType: "dot",
			gridColor: "gray",
			minimum: minimumValue
		},
		height: 300,
		data: [{
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "Confirmed",
			color: "red",
			dataPoints: confirmedData
		},
		{
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "SMA 4",
			color: "orange",
			dataPoints: sma1Data
		}, {
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "SMA 7",
			color: "blue",
			dataPoints: sma2Data
		}, {
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "SMA 14",
			color: "green",
			dataPoints: sma3Data
		}, {
			type: "spline",
			showInLegend: true,
			markerType: "none",
			legendText: "Active",
			color: "gray",
			dataPoints: acviteData
		}
		]
	}
	return (
		<div className="ChartWithLogarithmicAxis">
			<CanvasJSChart options={options} />
		</div>
	);

};

export default Chart;