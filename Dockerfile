# pull official base image
FROM node:10-alpine

# set working directory
WORKDIR /app

# add app
COPY . ./

RUN npm install

EXPOSE 3000

# start app
CMD ["npm", "start"]